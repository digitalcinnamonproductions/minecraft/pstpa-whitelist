package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.gui.entries.BooleanListEntry;
import me.shedaniel.clothconfig2.gui.entries.MultiElementListEntry;
import net.minecraft.text.Text;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Player;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_NAME;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_NAME_INVALID;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_NAME_TOOLTIP;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_NAME_TOO_LONG;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_NAME_TOO_SHORT;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_TOOLTIP_BLACKLIST;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_TOOLTIP_WHITELIST;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_UUID;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_UUID_INVALID;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_PLAYER_UUID_TOOLTIP;

public final class PlayerEntryBuilder {
    private final BooleanListEntry whitelistEntry;
    private final ConfigEntryBuilder entryBuilder;

    public PlayerEntryBuilder(ConfigEntryBuilder entryBuilder, BooleanListEntry whitelistEntry) {
        this.entryBuilder = entryBuilder;
        this.whitelistEntry = whitelistEntry;
    }

    private static Optional<Text> validUUID(String uuid) {
        if (!uuid.isEmpty()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                UUID.fromString(uuid);
            } catch (IllegalArgumentException ignored) {
                return Optional.of(PLAYERS_PLAYER_UUID_INVALID);
            }
        }
        return Optional.empty();
    }

    private static Optional<Text> validName(String name) {
        switch (Player.validateName(name)) {
            case INVALID_CHARACTERS:
                return Optional.of(PLAYERS_PLAYER_NAME_INVALID);
            case TOO_SHORT:
                return Optional.of(PLAYERS_PLAYER_NAME_TOO_SHORT);
            case TOO_LONG:
                return Optional.of(PLAYERS_PLAYER_NAME_TOO_LONG);
            case EMPTY:
            case VALID:
                return Optional.empty();
            default:
                throw new AssertionError();
        }
    }

    private static String getUuid(AtomicReference<Player> player) {
        UUID uuid = player.get().getUuid();
        return uuid == null ? "" : uuid.toString();
    }

    @SuppressWarnings("UnstableApiUsage")
    public MultiElementListEntry<AtomicReference<Player>> buildMultiElementListEntry(AtomicReference<Player> player) {
        AtomicReference<Player> playerReference = player == null ? new AtomicReference<>(new Player()) : player;

        MultiElementListEntry<AtomicReference<Player>> entry = new MultiElementListEntry<>(
                PLAYERS_PLAYER,
                playerReference,
                Arrays.asList(
                        entryBuilder.startStrField(PLAYERS_PLAYER_UUID, getUuid(playerReference))
                                .setDefaultValue("")
                                .setTooltip(PLAYERS_PLAYER_UUID_TOOLTIP)
                                .setErrorSupplier(PlayerEntryBuilder::validUUID)
                                .setSaveConsumer(uuid -> playerReference.set(playerReference.get().clone(uuid.isEmpty() ? null : UUID.fromString(uuid))))
                                .build(),
                        entryBuilder.startStrField(PLAYERS_PLAYER_NAME, playerReference.get().getName())
                                .setDefaultValue("")
                                .setTooltip(PLAYERS_PLAYER_NAME_TOOLTIP)
                                .setErrorSupplier(PlayerEntryBuilder::validName)
                                .setSaveConsumer(name -> playerReference.set(playerReference.get().clone(name)))
                                .build()
                ),
                true
        );

        entry.setTooltipSupplier(() -> Optional.of(new Text[]{whitelistEntry.getValue() ? PLAYERS_PLAYER_TOOLTIP_WHITELIST : PLAYERS_PLAYER_TOOLTIP_BLACKLIST}));

        return entry;
    }
}
