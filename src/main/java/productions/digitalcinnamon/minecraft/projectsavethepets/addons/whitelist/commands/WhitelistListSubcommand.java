package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import java.util.Set;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.serializers.ConfigSerializer;

public final class WhitelistListSubcommand {
    public static final LiteralArgumentBuilder<ServerCommandSource> LIST_SUBCOMMAND = CommandManager.literal("list")
            .executes(context -> {
                Set<String> playerNames = ConfigSerializer.get().getPlayerNames();

                if (playerNames.size() == 0) {
                    context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.none"), false);
                    return 1;
                }

                context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.list", playerNames.size(), String.join(", ", playerNames)), false);
                return playerNames.size();
            });
}
