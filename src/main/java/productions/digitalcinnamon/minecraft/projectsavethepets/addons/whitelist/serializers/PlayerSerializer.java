package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.serializers;

import blue.endless.jankson.JsonElement;
import blue.endless.jankson.JsonNull;
import blue.endless.jankson.JsonObject;
import blue.endless.jankson.JsonPrimitive;
import java.util.UUID;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.exceptions.ConfigurationException;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Player;

public final class PlayerSerializer {
    private static String getName(JsonElement element) throws ConfigurationException {
        if (element instanceof JsonNull) {
            return null;
        }

        if (!(element instanceof JsonPrimitive) || !(((JsonPrimitive) element).getValue() instanceof String)) {
            throw new ConfigurationException("name is not of type String");
        }

        return (String) ((JsonPrimitive) element).getValue();
    }

    private static String getUuidString(JsonElement element) throws ConfigurationException {
        if (element == null || element instanceof JsonNull) {
            return null;
        }

        if (!(element instanceof JsonPrimitive) || !(((JsonPrimitive) element).getValue() instanceof String)) {
            throw new ConfigurationException("uuid is not of type String");
        }

        return (String) ((JsonPrimitive) element).getValue();
    }

    public static Player toPlayer(JsonObject playerJson) throws ConfigurationException {
        String name = getName(playerJson.get("name"));
        String uuidString = getName(playerJson.get("uuid"));

        if (name == null && uuidString == null) {
            return null;
        }

        switch (Player.validateName(name)) {
            case INVALID_CHARACTERS:
                throw new ConfigurationException("name, '" + name + "', contains invalid characters");
            case TOO_SHORT:
                throw new ConfigurationException("name, '" + name + "', is too short");
            case TOO_LONG:
                throw new ConfigurationException("name, '" + name + "', is too long");
            case EMPTY:
                throw new ConfigurationException("name is empty");
            case VALID:
                break;
            default:
                throw new AssertionError();
        }

        UUID uuid;

        try {
            uuid = uuidString == null ? null : UUID.fromString(uuidString);
        } catch (IllegalArgumentException iae) {
            if (name == null) {
                throw new ConfigurationException("uuid is invalid and name is null", iae);
            }

            return new Player(null, name);
        }

        return new Player(uuid, name == null ? "" : name);
    }

    public static JsonObject toJsonObject(Player player) {
        JsonObject object = new JsonObject();

        String name = player.getName();
        UUID uuid = player.getUuid();

        object.put("name", name == null ? JsonNull.INSTANCE : new JsonPrimitive(name));
        object.put("uuid", uuid == null ? JsonNull.INSTANCE : new JsonPrimitive(uuid.toString()));

        return object;
    }
}
