package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.gui.entries.BooleanListEntry;
import me.shedaniel.clothconfig2.gui.entries.MultiElementListEntry;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.Whitelist;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Config;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.serializers.ConfigSerializer;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.exceptions.ConfigurationException;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Player;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.CATEGORY_GENERAL;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_TOOLTIP_BLACKLIST;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.PLAYERS_TOOLTIP_WHITELIST;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.TITLE;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.WHITELIST;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.WHITELIST_FALSE;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.WHITELIST_TOOLTIP;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigTextConstants.WHITELIST_TRUE;

public final class ConfigScreen {
    public static Screen createThing(Screen parent) {
        ConfigBuilder configBuilder = ConfigBuilder.create();
        ConfigCategory configCategory = configBuilder.getOrCreateCategory(CATEGORY_GENERAL);
        ConfigEntryBuilder entryBuilder = configBuilder.entryBuilder();
        Config config = ConfigSerializer.get();

        BooleanListEntry whitelistEntry = entryBuilder.startBooleanToggle(WHITELIST, config.isWhitelist())
                .setDefaultValue(true)
                .setTooltip(WHITELIST_TOOLTIP)
                .setYesNoTextSupplier(whitelist -> whitelist ? WHITELIST_TRUE : WHITELIST_FALSE)
                .build();

        PlayerEntryBuilder playerEntryBuilder = new PlayerEntryBuilder(entryBuilder, whitelistEntry);

        NestedListListEntry<AtomicReference<Player>, MultiElementListEntry<AtomicReference<Player>>> playersEntry = new NestedListListEntry<>(
                PLAYERS,
                config.getPlayers().stream().map(AtomicReference::new).collect(Collectors.toList()),
                true,
                Collections::emptyList,
                entryBuilder.getResetButtonKey(),
                true,
                true,
                (player, entry) -> playerEntryBuilder.buildMultiElementListEntry(player));

        playersEntry.setTooltipSupplier(() -> Optional.of(new Text[]{whitelistEntry.getValue() ? PLAYERS_TOOLTIP_WHITELIST : PLAYERS_TOOLTIP_BLACKLIST}));


        configCategory.addEntry(whitelistEntry).addEntry(playersEntry);

        return configBuilder.setTitle(TITLE)
                .setSavingRunnable(() -> {
                    try {
                        ConfigSerializer.save(new Config(whitelistEntry.getValue(), playersEntry.getValue().stream()
                                .map(AtomicReference::get)
                                .collect(Collectors.toSet()))
                                .normalize());
                    } catch (ConfigurationException ce) {
                        Whitelist.LOGGER.error("Failed to save the config", ce);
                    }
                })
                .setParentScreen(parent)
                .build();
    }
}
