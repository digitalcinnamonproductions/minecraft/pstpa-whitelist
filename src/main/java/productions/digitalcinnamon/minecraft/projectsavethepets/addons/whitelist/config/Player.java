package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import productions.digitalcinnamon.minecraft.projectsavethepets.PlayerCache;

public final class Player {
    private static final List<Character> validCharacters = new ArrayList<>();

    static {
        for (char i = 'a'; i <= 'z'; i++) {
            validCharacters.add(i);
        }

        for (char i = 'A'; i <= 'Z'; i++) {
            validCharacters.add(i);
        }

        for (char i = '0'; i <= '9'; i++) {
            validCharacters.add(i);
        }

        validCharacters.add('_');
    }

    private final UUID uuid;
    private final String name;
    private final UUID offlineUuid;

    public Player(UUID uuid, String name) {
        UUID offlineUuid = PlayerEntity.getOfflinePlayerUuid(name);

        this.uuid = offlineUuid.equals(uuid) ? null : uuid;
        this.name = name;
        this.offlineUuid = offlineUuid;
    }

    public Player() {
        this(null, "");
    }

    public static PlayerNameErrorTypes validateName(String name) {
        if (name == null) return PlayerNameErrorTypes.VALID;

        if (!name.chars().allMatch(character -> validCharacters.contains((char) character))) {
            return PlayerNameErrorTypes.INVALID_CHARACTERS;
        }

        if (name.isEmpty()) {
            return PlayerNameErrorTypes.EMPTY;
        }

        if (name.length() < 3) {
            return PlayerNameErrorTypes.TOO_SHORT;
        }

        if (name.length() > 16) {
            return PlayerNameErrorTypes.TOO_LONG;
        }

        return PlayerNameErrorTypes.VALID;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public UUID getOfflineUuid() {
        return offlineUuid;
    }

    public Player normalize() {
        if (uuid == null) {
            if (name.isEmpty()) {
                return null;
            }

            UUID uuid = PlayerCache.getUUID(name);

            if (uuid == null) {
                return null;
            }

            return clone(uuid);
        }

        String name = PlayerCache.getUsername(uuid);

        if (name == null) {
            return null;
        }

        return clone(name);
    }

    @Override
    public String toString() {
        return "Player{" +
                "uuid=" + uuid +
                ", name='" + name + '\'' +
                ", offlineUuid=" + offlineUuid +
                '}';
    }

    public Player clone(String name) {
        return new Player(uuid, name);
    }

    public Player clone(UUID uuid) {
        return new Player(uuid, name);
    }

    public enum PlayerNameErrorTypes {
        VALID,
        INVALID_CHARACTERS,
        TOO_LONG,
        TOO_SHORT,
        EMPTY
    }
}
