package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.serializers;

import blue.endless.jankson.Jankson;
import blue.endless.jankson.JsonArray;
import blue.endless.jankson.JsonElement;
import blue.endless.jankson.JsonObject;
import blue.endless.jankson.JsonPrimitive;
import blue.endless.jankson.impl.SyntaxError;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import productions.digitalcinnamon.minecraft.projectsavethepets.ProjectSaveThePets;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Config;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.exceptions.ConfigurationException;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Player;

public final class ConfigSerializer {
    private static final File CONFIG_FILE = new File(ProjectSaveThePets.CONFIG_FOLDER, "whitelist.json5");
    private static final Jankson JANKSON = Jankson.builder().build();
    private static final AtomicReference<Config> CONFIG = new AtomicReference<>(new Config());

    public static void save(Config config) throws ConfigurationException {
        CONFIG.set(config);

        JsonObject configJson = new JsonObject();
        JsonArray playersJson = new JsonArray();

        playersJson.addAll(config.getPlayers().stream().map(PlayerSerializer::toJsonObject).collect(Collectors.toList()));

        if (playersJson.isEmpty()) {
            playersJson.add(PlayerSerializer.toJsonObject(new Player(null, null)));
        }

        configJson.put("whitelist", new JsonPrimitive(config.isWhitelist()), "Toggle between a whitelist and blacklist");
        configJson.put("players", playersJson, "The players to whitelist/blacklist");
        configJson.toJson(true, true, 2);

        try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(CONFIG_FILE))) {
            osw.write(configJson.toJson(true, true, 2));
        } catch (IOException ioe) {
            throw new ConfigurationException("Failed to write the config", ioe);
        }
    }

    public static void load() throws ConfigurationException {
        if (!CONFIG_FILE.exists()) {
            Config config = new Config();

            save(config);

            return;
        }

        JsonObject configJson = readConfig();
        JsonPrimitive whitelistJson = (JsonPrimitive) configJson.get("whitelist");
        JsonArray playersJson = (JsonArray) configJson.get("Players");

        boolean whitelist = whitelistJson == null || JsonPrimitive.TRUE.equals(whitelistJson);
        Set<Player> players = new HashSet<>();

        if (playersJson != null) {
            for (JsonElement playerJson : playersJson) {
                Player player = PlayerSerializer.toPlayer((JsonObject) playerJson);

                if (player != null) {
                    players.add(player);
                }
            }
        }

        save(new Config(whitelist, players).normalize());
    }

    private static JsonObject readConfig() throws ConfigurationException {
        try {
            return JANKSON.load(CONFIG_FILE);
        } catch (IOException ioe) {
            throw new ConfigurationException("Failed to read the config", ioe);
        } catch (SyntaxError se) {
            throw new ConfigurationException("Failed to parse the config", se);
        }
    }

    public static Config get() {
        return CONFIG.get();
    }
}
