package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings("CanBeFinal")
public final class Config {
    private final boolean whitelist;
    private final Set<Player> players;
    private final Set<String> playerNames;
    private final Set<UUID> playerUuids;
    private final Set<UUID> offlinePlayerUuids;

    public Config(boolean whitelist, Set<Player> players) {
        this.whitelist = whitelist;
        this.players = Collections.unmodifiableSet(players);
        this.playerNames = Collections.unmodifiableSet(players.stream().map(Player::getName).collect(Collectors.toSet()));
        this.playerUuids = Collections.unmodifiableSet(players.stream().map(Player::getUuid).filter(Objects::nonNull).collect(Collectors.toSet()));
        this.offlinePlayerUuids = Collections.unmodifiableSet(players.stream().map(Player::getOfflineUuid).collect(Collectors.toSet()));
    }

    @Override
    public String toString() {
        return "Config{" +
                "whitelist=" + whitelist +
                ", players=" + players +
                '}';
    }

    public Config() {
        this(true, Collections.emptySet());
    }

    public boolean isWhitelist() {
        return whitelist;
    }

    public Config normalize() {
        return new Config(whitelist, players.stream()
                .map(Player::normalize)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public Set<String> getPlayerNames() {
        return playerNames;
    }

    public Set<UUID> getPlayerUuids() {
        return playerUuids;
    }

    public Set<UUID> getOfflinePlayerUuids() {
        return offlinePlayerUuids;
    }

    public boolean isWhitelisted(UUID player) {
        return playerUuids.contains(player) || offlinePlayerUuids.contains(player);
    }

    public Config clone(boolean whitelist) {
        return new Config(whitelist, players);
    }

    public Config clone(Set<Player> players) {
        return new Config(whitelist, players);
    }
}
