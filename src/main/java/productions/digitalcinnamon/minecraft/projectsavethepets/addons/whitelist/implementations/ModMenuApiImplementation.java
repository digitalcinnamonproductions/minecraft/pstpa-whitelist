package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.implementations;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui.ConfigScreen;

@SuppressWarnings("unused")
public final class ModMenuApiImplementation implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return ConfigScreen::createThing;
    }
}
