package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.gui;

import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public final class ConfigTextConstants {
    public static final Text TITLE = new TranslatableText("config.projectsavethepets.whitelist.title");
    public static final Text CATEGORY_GENERAL = new TranslatableText("config.projectsavethepets.whitelist.general");

    public static final Text PLAYERS = new TranslatableText("config.projectsavethepets.whitelist.option.players");
    public static final Text PLAYERS_TOOLTIP_BLACKLIST = new TranslatableText("config.projectsavethepets.whitelist.option.players.tooltip.blacklist");
    public static final Text PLAYERS_TOOLTIP_WHITELIST = new TranslatableText("config.projectsavethepets.whitelist.option.players.tooltip.whitelist");

    public static final Text PLAYERS_PLAYER = new TranslatableText("config.projectsavethepets.whitelist.option.players.player");
    public static final Text PLAYERS_PLAYER_TOOLTIP_BLACKLIST = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.tooltip.blacklist");
    public static final Text PLAYERS_PLAYER_TOOLTIP_WHITELIST = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.tooltip.whitelist");

    public static final Text PLAYERS_PLAYER_NAME = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.name");
    public static final Text PLAYERS_PLAYER_NAME_TOOLTIP = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.name.tooltip");
    public static final Text PLAYERS_PLAYER_NAME_INVALID = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.name.invalid");
    public static final Text PLAYERS_PLAYER_NAME_TOO_LONG = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.name.too_long");
    public static final Text PLAYERS_PLAYER_NAME_TOO_SHORT = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.name.too_short");

    public static final Text PLAYERS_PLAYER_UUID = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.uuid");
    public static final Text PLAYERS_PLAYER_UUID_INVALID = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.uuid.invalid");
    public static final Text PLAYERS_PLAYER_UUID_TOOLTIP = new TranslatableText("config.projectsavethepets.whitelist.option.players.player.uuid.tooltip");

    public static final Text WHITELIST = new TranslatableText("config.projectsavethepets.whitelist.option.whitelist");
    public static final Text WHITELIST_FALSE = new TranslatableText("config.projectsavethepets.whitelist.option.whitelist.false");
    public static final Text WHITELIST_TRUE = new TranslatableText("config.projectsavethepets.whitelist.option.whitelist.true");
    public static final Text WHITELIST_TOOLTIP = new TranslatableText("config.projectsavethepets.whitelist.option.whitelist.tooltip");
}
