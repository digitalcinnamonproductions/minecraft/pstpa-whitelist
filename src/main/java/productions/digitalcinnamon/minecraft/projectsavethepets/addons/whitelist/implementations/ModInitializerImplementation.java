package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.implementations;

import net.fabricmc.api.ModInitializer;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.Whitelist;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.serializers.ConfigSerializer;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.exceptions.ConfigurationException;

@SuppressWarnings("unused")
public class ModInitializerImplementation implements ModInitializer {
    @Override
    public void onInitialize() {
        try {
            ConfigSerializer.load();
        } catch (ConfigurationException e) {
            Whitelist.LOGGER.error("Failed to load the config");
        }
    }
}
