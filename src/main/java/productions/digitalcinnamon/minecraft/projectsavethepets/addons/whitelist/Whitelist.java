package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist;

import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Config;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.serializers.ConfigSerializer;
import productions.digitalcinnamon.minecraft.projectsavethepets.apis.ProjectSaveThePetsApi;

@SuppressWarnings("unused")
public final class Whitelist implements ProjectSaveThePetsApi {
    public static final Logger LOGGER = LogManager.getFormatterLogger("PStPA-W");

    @Override
    public boolean shouldBlockDamage(UUID ownerUuid, PlayerEntity attacker) {
        Config config = ConfigSerializer.get();
        return config.isWhitelisted(ownerUuid) == config.isWhitelist();
    }

//    @Override
//    public void onInitialize() {
//        CommandRegistrationCallback.EVENT.register((dispatcher, dedicated) -> dispatcher.register(literal("pstp-whitelist")
//                .requires((serverCommandSource) -> serverCommandSource.hasPermissionLevel(3))
//                .then(literal("list").executes(context -> {
//                    if (playerNames.size() == 0) {
//                        context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.none"), false);
//                    } else {
//                        context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.list", playerNames.size(), String.join(", ", playerNames)), false);
//                    }
//
//                    return playerNames.size();
//                }))
//                .then(literal("add").then(
//                        argument("players", gameProfile()).suggests((context, builder) -> {
//                            PlayerManager playerManager = context.getSource().getMinecraftServer().getPlayerManager();
//
//                            return CommandSource.suggestMatching(
//                                    playerManager.getPlayerList().stream()
//                                            .filter((serverPlayerEntity) -> !playerUUIDs.contains(serverPlayerEntity.getUuid().toString()))
//                                            .map((serverPlayerEntity) -> serverPlayerEntity.getGameProfile().getName())
//                                    , builder);
//                        }).executes(context -> {
//                            Collection<GameProfile> profiles = getProfileArgument(context, "players");
//
//                            Set<Player> players = profiles.stream()
//                                    .filter(profile -> !playerNames.contains(profile.getName()))
//                                    .map(profile -> new Player(profile.getId().toString(), profile.getName()))
//                                    .collect(Collectors.toSet());
//
//                            if (players.size() == 0) {
//                                context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.none"), false);
//                            } else {
//                                configHolder.get().players.addAll(players);
//                                configHolder.save();
//
//                                context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.add",
//                                        players.size(),
//                                        String.join(", ", (CharSequence) players.stream()
//                                                .map(player -> player.name)
//                                                .collect(Collectors.toSet())
//                                        )
//                                ), false);
//                            }
//
//                            return profiles.size();
//                        }))
//                )
//                .then(literal("remove").then(
//                        argument("players", gameProfile())
//                                .suggests((context, builder) -> CommandSource.suggestMatching(playerNames, builder))
//                                .executes(context -> {
//                                    Collection<GameProfile> profiles = getProfileArgument(context, "players");
//
//                                    Set<Player> players = profiles.stream()
//                                            .filter(profile -> playerNames.contains(profile.getName()))
//                                            .map(profile -> new Player(profile.getId().toString(), profile.getName()))
//                                            .collect(Collectors.toSet());
//
//                                    if (players.size() == 0) {
//                                        context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.none"), false);
//                                    } else {
//                                        configHolder.get().players.removeAll(players);
//                                        configHolder.save();
//
//                                        context.getSource().sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.remove",
//                                                players.size(),
//                                                String.join(", ", (CharSequence) players.stream()
//                                                        .map(player -> player.name)
//                                                        .collect(Collectors.toSet())
//                                                )
//                                        ), false);
//                                    }
//
//                                    return profiles.size();
//                                })
//                        )
//                )
//        ));
//    }
}
