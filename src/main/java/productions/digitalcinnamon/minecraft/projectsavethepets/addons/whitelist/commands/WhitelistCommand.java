package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import net.minecraft.command.CommandSource;
import net.minecraft.server.command.CommandManager;
import net.minecraft.text.TranslatableText;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.exceptions.ConfigurationException;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.commands.WhitelistAddSubcommand.ADD_SUBCOMMAND;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.commands.WhitelistListSubcommand.LIST_SUBCOMMAND;

public final class WhitelistCommand {
    public static final DynamicCommandExceptionType CONFIGURATION_EXCEPTION = new DynamicCommandExceptionType(o -> new TranslatableText("commands.whitelist.add.failed", ((ConfigurationException) o).getMessage()));


    private static final LiteralArgumentBuilder<CommandSource> WHITELIST_COMMAND = CommandManager.literal("pstpa-whitelist")
            .requires(source -> source.hasPermissionLevel(3))
            .then(LIST_SUBCOMMAND)
            .then(ADD_SUBCOMMAND)
            .then(getRemoveSubcommand())
            .then(getReloadSubcommand());


}
