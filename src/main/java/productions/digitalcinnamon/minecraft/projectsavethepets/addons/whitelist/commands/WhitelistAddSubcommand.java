package productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.commands;

import com.mojang.authlib.GameProfile;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import net.minecraft.command.CommandSource;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Config;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.config.Player;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.exceptions.ConfigurationException;
import productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.serializers.ConfigSerializer;
import static net.minecraft.command.argument.GameProfileArgumentType.gameProfile;
import static net.minecraft.command.argument.GameProfileArgumentType.getProfileArgument;
import static net.minecraft.server.command.CommandManager.argument;
import static productions.digitalcinnamon.minecraft.projectsavethepets.addons.whitelist.commands.WhitelistCommand.CONFIGURATION_EXCEPTION;

public final class WhitelistAddSubcommand {
    public static final LiteralArgumentBuilder<ServerCommandSource> ADD_SUBCOMMAND = CommandManager.literal("list")
            .then(argument("players", gameProfile())
                    .suggests(WhitelistAddSubcommand::addSuggestions)
                    .executes(WhitelistAddSubcommand::run));

    private static CompletableFuture<Suggestions> addSuggestions(CommandContext<ServerCommandSource> context, SuggestionsBuilder builder) {
        PlayerManager playerManager = context.getSource().getMinecraftServer().getPlayerManager();
        Set<UUID> playerUUIDs = ConfigSerializer.get().getPlayerUuids();

        return CommandSource.suggestMatching(
                playerManager.getPlayerList().stream()
                        .filter((serverPlayerEntity) -> !playerUUIDs.contains(serverPlayerEntity.getUuid()))
                        .map((serverPlayerEntity) -> serverPlayerEntity.getGameProfile().getName()),
                builder);
    }

    private static int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        Collection<GameProfile> profiles = getProfileArgument(context, "players");
        Config config = ConfigSerializer.get();
        Set<String> playerNames = config.getPlayerNames();
        ServerCommandSource source = context.getSource();

        Set<Player> players = profiles.stream()
                .filter(profile -> !playerNames.contains(profile.getName()))
                .map(profile -> new Player(profile.getId(), profile.getName()))
                .collect(Collectors.toSet());

        if (players.size() == 0) {
            source.sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.add.failed"), false);
            return 1;
        }

        players.forEach(player -> source.sendFeedback(new TranslatableText("commands.projectsavethepets.whitelist.add.success", player.getName()), false));

        try {
            ConfigSerializer.save(config.clone(players.addAll(config.getPlayers())));
        } catch (ConfigurationException e) {
            throw CONFIGURATION_EXCEPTION.create(e);
        }

        return profiles.size();
    }
}
