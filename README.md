# Project: Save the Pets! Addon - Vanilla Teams
**Version 1.0.0**  
**Client side**  
**Server side**

An addon for Project: Save the Pets! that allows players in the same vanilla team
(aka scoreboard team) with friendly fire disabled to not harm their teammates' pets.

Works on both Client and Server side!

## Dependencies

* [Project: Save the Pets!](https://www.curseforge.com/minecraft/mc-mods/projectsavethepets)
* [Fabric Mod Loader](https://fabricmc.net/)

### Minecraft Versions
This mod should work with 1.16.x, the 1.17 snapshots, and the future releases of 1.17.x. No update or different file
needed!

No older versions will be supported.
